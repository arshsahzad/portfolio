---
title: "Windfolio"
github: "https://git.webnizo.com/arshsahzad/Windfolio"
external: "https://www.arsh.dev/link/windfolio/"
image: "https://www.arsh.dev/file/photo/windfolio.png"
tech:
  - UIKit
  - Quill.js
  - React.js
  - Fluent UI
  - React Draggable
---

Created a web-based portfolio, It replicates the features of windows 10 theme and it is deployed on the netlify using tech stack such as node.js, react.js & react draggable.
