---
title: "LinkTree"
github: "https://git.webnizo.com/arshsahzad/LinkTree"
external: "https://www.arsh.dev/link/linktree/"
image: "https://www.arsh.dev/file/photo/linktree.png"
tech:
  - CSS
  - HTML
  - Canvas
  - Javascript
  - FontAwesome
---

An alternative platform for creating a personalized easily customizable page, that houses all the important links I want to share with the internet. It is built using HTML, CSS and JS.
