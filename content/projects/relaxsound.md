---
title: "RelaxSound"
github: "https://git.webnizo.com/arshsahzad/RelaxSound"
external: "https://www.arsh.dev/link/relaxsound/"
image: "https://www.arsh.dev/file/photo/relaxsound.png"
tech:
  - SCSS
  - Next.js
  - Node.js
  - SoundBible
  - FontAwesome
---

Ambient sound healing therapy uses aspects of nature's sounds to improve physical and mental health. It is based on next.js and prerecorded sounds of nature from SoundBible.
