---
title: "QuizBuzz"
github: "https://git.webnizo.com/arshsahzad/QuizBuzz"
external: "https://www.arsh.dev/link/quizbuzz/"
image: "https://www.arsh.dev/file/photo/quizbuzz.png"
tech:
  - CSS
  - HTML
  - Netlify
  - Javascript
  - FontAwesome
---

An app based on HTML, CSS & JS with the feature of a 30-second timer for each of five questions and reports is generated based on the performance of the participated user.
