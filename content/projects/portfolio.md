---
title: "Portfolio"
github: "https://git.webnizo.com/arshsahzad/Portfolio"
external: "https://www.arsh.dev/link/portfolio/"
image: "https://www.arsh.dev/file/photo/portfolio.png"
tech:
  - Gatsby
  - Node.js
  - Chart.js
  - React.js
  - Framer-Motion
---

Built using Gatsby that combines functionality from React, GraphQL and Webpack. It uses JS libraries such as chart.js for data visualization & framer-motion for react animation.
