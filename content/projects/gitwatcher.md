---
title: "GitWatcher"
github: "https://git.webnizo.com/arshsahzad/GitWatcher"
external: "https://www.arsh.dev/link/gitwatcher/"
image: "https://www.arsh.dev/file/photo/gitwatcher.png"
tech:
  - Node.js
  - React.js
  - Javascript
  - HTML & CSS
  - Axios Library
---

Developed a github profile analysis tool that gets the information of github users like total public repos, forks. It fetches data from github using axios library for http requests.
