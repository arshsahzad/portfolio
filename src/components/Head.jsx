import React from 'react';
import { Helmet } from 'react-helmet';

function Head() {
    return (
        <Helmet title="Arsh Sahzad - Portfolio" />
    )
}

export default Head;