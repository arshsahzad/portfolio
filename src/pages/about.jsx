import React, { useState } from 'react';
import { motion } from 'framer-motion';
import { Doughnut } from 'react-chartjs-2';

import Navbar from '../components/Navbar';
import Head from '../components/Head';

function AboutPage({ location }) {
    const [open, setOpen] = useState(false);
    return (
        <>
            <Head />
            <div className="h-screen w-screen bg-primary font-mono">
                <Navbar open={open} setOpen={setOpen} location={location} />
                <div className={`flex bg-primary ${open ? 'mt-44' : ''}`}>
                    <div className="p-10 text-info md:p-28">
                        <motion.h1
                            initial={{ y: 20, opacity: 0 }}
                            animate={{ y: 0, opacity: 1 }}
                            transition={{delay: 0.5}}
                            className="text-2xl font-bold md:text-4xl"
                        >
                            About
                        </motion.h1>
                        <div className="flex flex-row my-8 justify-between">
                            <div className="w-5/6">
                                <motion.p
                                    initial={{ y: 20, opacity: 0 }}
                                    animate={{ y: 0, opacity: 1 }}
                                    transition={{delay: 1}}
                                >
                                    Hello, My name is Arsh Sahzad <i>aka <del>Md Amir Ali</del></i>, a recent B.Tech graduate from GNIOT,
                                    Greater Noida, India. I'm seeking a challenging yet rewarding job position in a progressive environment
                                    where learning, innovation and creativity are encouraged, that will provide me an opportunity to utilize 
                                    my technical skills and abilities in an engineering capacity.
                                </motion.p>
                                <br/>
                                <motion.p
                                    initial={{ y: 20, opacity: 0 }}
                                    animate={{ y: 0, opacity: 1 }}
                                    transition={{delay: 1.5}}
                                >
                                    I love working on a variety of technologies including such as Software Development, Web Development, Digital
                                    Marketing and Cloud Computing. My goal is to build highly performant applications that solve real-world problems
                                    and provide users with an awesome experience. A bit about me, I love music, arts and innovative tech.
                                </motion.p>
                                <br/>
                                <motion.div
                                    initial={{ y: 20, opacity: 0 }}
                                    animate={{ y: 0, opacity: 1 }}
                                    transition={{delay: 2}}
                                >
                                    <p>Here are a few technologies that I work with:</p>
                                    <br/>
                                    <div className="flex flex-row w-full justify-around">
                                    <ul className="list-disc">
                                            <li>HTML & CSS</li>
                                            <li>Node.js</li>
                                            <li>Python</li>
                                        </ul>
                                        <ul className="list-disc">
                                            <li>Javascript</li>
                                            <li>React.js</li>
                                            <li>Django</li>
                                        </ul>
                                    </div>
                                </motion.div>
                            </div>
                            <motion.div 
                                initial={{ y: 20, opacity: 0 }}
                                animate={{ y: 0, opacity: 1 }}
                                transition={{delay: 2.5}}
                                className="w-1/3 hidden lg:block"
                            >
                                <Doughnut
                                    options={{
                                        maintainAspectRatio: false,
                                        legend: {
                                            position: 'right'
                                        },
                                        title: {
                                            display: true,
                                            text: "Top languages (Based on lines of code written)",
                                            fontColor: "#D4EAE9",
                                            position: "bottom"
                                        },
                                    }}
                                    data={{
                                        labels: [
                                            "Shell",
                                            "Javascript",
                                            "Java & PHP",
                                            "HTML & CSS",
                                            "Node & React",
                                            "Python & Django"
                                        ],
                                        datasets: [
                                            {
                                                data: [
                                                    5,
                                                    20,
                                                    10,
                                                    20,
                                                    35,
                                                    10
                                                ],
                                                backgroundColor: [
                                                    'rgb(209, 140, 29)', // shell
                                                    'rgb(217, 214, 26)', // javascript
                                                    'rgb(104, 112, 117)', // java & php
                                                    'rgb(29, 137, 209)', // html & css
                                                    'rgb(131, 79, 214)', // node & react
                                                    'rgb(237, 78, 50)', // python & django
                                                ],
                                                borderColor: [
                                                    'rgb(209, 140, 29)', // shell
                                                    'rgb(217, 214, 26)', // javascript
                                                    'rgb(104, 112, 117)', // java & php
                                                    'rgb(29, 137, 209)', // html & css
                                                    'rgb(131, 79, 214)', // node & react
                                                    'rgb(237, 78, 50)', // python & django
                                                ]
                                            }
                                        ]
                                    }}
                                />
                            </motion.div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default AboutPage;